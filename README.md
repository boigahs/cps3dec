# cps3dec
Tool to decrypt CPS3 roms. Supports old FBA (10, 20...) and current MAME (simm1.0, simm1.1...) formats. One ANSI C file for maximum compatibility. Should work on any computer imagineable.

## Usage
```
cps3dec [-o | -n] [gameid] [out] [files]
cps3dec -o [gameid] [out] [10 file] [20 file]
cps3dec -n [gameid] [out] [simm1.0 file] [simm1.1 file] [simm1.2 file] [simm1.3 file] [simm2.0 file] [simm2.1 file] [simm2.2 file] [simm2.3 file]
```

### Flags
* -o: Decrypt old FBA roms
* -n: Decrypt current MAME roms

### Arguments
* gameid: Support IDs are: sfiiin, sfiiina, jojon, jojonr1, jojonr2, sfiii3n, sfiii3nr1, jojoban, jojobane, jojobanr1, jojobaner1
* out: Filename for the decrypted code
* files: See usage.

## Notes
* Currently, this tool works with files from NO CD dumps.
* Windows users, I'm sorry, but you're gonna have to type out all of those simm filenames because your shells do not support wildcard expansion. Consider writing a batch script, installing Cygwin (or similar), or uninstalling Windows.
